# cellv1.5





## Name
Toto-Cell V1.5.1

## Description
Toto-Cell is a software package that represents the evolution of a cell population after a video-microscopy experiment through a cladogram and provides statistical data on this evolution.


## Installation
A CmakeLists.txt is provide with the source code and allows to easily compile the applicaion under Linux. Commands are :

`cmake .`

`make`

The application is then compiled in the `bin` directory. Note that executables are provided for linux and windows in the bin directory of the repository.

## Usage
See the user's manual available in the `doc` directory.

## Support
For any question please send an email to christophe.renaud@univ-littoral.fr.

## Roadmap
Two developments will be continued:

* the addition of a small graphical interface, to make it easier to use the software without going through a command line

* the ability to compare different cladograms to highlight the differences between them




## Authors and acknowledgment

Author : christophe Renaud 

Thanks to Samuel Delepoulle for developing a web interface so that you can use Toto-Cell online : https://diran.univ-littoral.fr/web-cell/cell.html

## License

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.


