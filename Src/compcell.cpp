/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include "./libclado/Cladogram.hpp"

/**
 * comp-cell is a software that allows you to read back data
 * from two cell evolution sequence analysis and compare them. 
 */


#define VERSION "1.0"
// build from version 1.5 of toto-cell


// pre-defining functions
void copyright();

/**
 * main function. 
 * @param argc number of arguments of the command (must be 2)
 * @param argv list of arguments of the commands. 
 * The syntax of the command must be : <commandName>  <event file name>
 */
int main(int argc, char *argv[]){
  // printing the copyright
  copyright();
  
  //verifying parameters
  if(argc != 3){
    cerr << "Syntax = " << argv[0] << " <event file name1> <event file name2>" << endl;;
    return 0;
  }


 
  // building the two cladograms from the data
  Cladogram clado[2];
  clado[0].load(argv[1]);
  clado[1].load(argv[2]);


  // end message
  cout << "Files correctly loaded" << endl;

  // compar the cladograms
  if(clado[0].compare(clado[1]))
    cout << "identiques" << endl;
  else
    cout << "différents" << endl;
  
  return 1;
}


/**
 * Function that prints the toto cell copyright
 */
void copyright(){
  cout << "Compcell V" << VERSION;
  cout << " - Copyright C. RENAUD 2023" << endl;
}
