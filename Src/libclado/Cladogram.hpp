/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CLADOGRAM_HPP
#define CLADOGRAM_HPP

#include <string>
#include <fstream>
using namespace std;

#include "Node.hpp"

class Cladogram {
private:
  vector <Name> names; /**< dictionary of cell names */
  vector <Node *> cells;/**< list of cell trees */
  string color; /**< Display color */
  int maxTime; /**< higher event date */
  int timeInterval; /**< time interval between each frame (in minute) */
public:
  Cladogram();
  ~Cladogram();

  /**
   * Read the cells events file and build the corresponding cladogram.
   * @param filename the name of the event file
   * @return false if any error occurs, true otherwise
   */
  bool load(const string &filename);

  /**
   * Function allowing the graphic output in SVG format of the different
   * trees representing the evolution of the cells.
   * @param filename name of the save file in SVG format
   * @param generator 
   */
  void print(const string &filename, const string &generator);

  /**
   * Function allowing to export cladogram's statistics to 
   * a CSV file
   * @param csvfilename name of the stats file in CSV format
   */
  void printStats(const string &csvfilename);

  /**
   * Compare whether two cladograms are identical or not
   * @param c the cladogram to compare with
   * @param true if the cladogras are identical, false otherwise
   */
  bool compare(const Cladogram &c);
  
private:
  string getLogFilename(const string &evtfilename);
  string getSvgFilename(const string &evtfilename);
  string getCsvFilename(const string &evtfilename);

  /**
   * This function ensures the execution of all the operations required
   * when the begin keyword
   * is encountered in the cell event file.
   * @param in is the input filestream of the events
   * @param time is the date the "begin" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void begin(ifstream &in, int time, bool log, ofstream &out);
  /**
   * This function ensures the execution of all the operations required when the end keyword
   * is encountered in the cell event file.
   * @param in is the input filestream of the events
   * @param time is the date the "end" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void end(ifstream &in, int time, bool log, ofstream &out);
  /**
   * This function ensures the execution of all the operations required when the out keyword
   * is encountered in the cell event file.
   * @param in is the input filestream of the events
   * @param time is the date the "out" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void out(ifstream &in, int time, bool log, ofstream &output);
  /**
   * This function ensures the execution of all the operations required when the death keyword
   * is encountered in the cell event file.
   * @param in is the input filestream of the events
   * @param time is the date the "death" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void death(ifstream &in, int time, bool log, ofstream &out);
  /**
   * This function ensures the execution of all the operations required when the div keyword
   * is encountered in the cell event file. The syntax of this keyword is: T div Name N name1 name2 ... nameN
   * @param in is the input filestream of the events
   * @param time is the date the "div" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void div(ifstream &in, int time, bool log, ofstream &out);
  /**
   * This function ensures the execution of all the operations required when the fusion keyword
   * is encountered in the cell event file. 
   * @param in is the input filestream of the events
   * @param time is the date the "fusion" event occurs
   * @param log if true allows the output in the log file
   * @param out is the output filestreams of the log file
   */
  void fusion(ifstream &in, int time, bool log, ofstream &out);
  
  /**
   *  verify whether a name exists in the table of names
   * @return : -1 if name does not exist, its location otherwise
   */
  int exist(const string &name);
  /**
   * Verify whether a string is composed only of digit
   * Such a string should represent a time.
   * @return true if only digits are present in the string, false otherwise
   */
  bool isTime(const string &s);

};
#endif
