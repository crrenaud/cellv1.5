/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _STAT_HPP
#define _STAT_HPP

class Node;
class Name;

#include <string>
#include <vector>
#include "Node.hpp"

/**
 * This clas is used to represent a node that must wait for some additional
 * information before its statistics can be computed. This is required when
 * the node is the son of several fusionned cells.
 */
class WaitingStat {
public:
  Node *pcell; // pointer to the waiting fusionned cell
  int nbParentsEncountered;// count the number of parents that have yet be encountered
public:
  WaitingStat();
  WaitingStat(Node *pcell);
  Node* getCell(){ return pcell;}
  bool isready();
  friend ostream& operator<<(ostream& out, const WaitingStat &e);
};

/**
 * This class is used to manage a pending data set when collecting statistics.
 */
class WaitingStatList {
private:
  vector <WaitingStat> elt; // the list of waiting data
public:
  WaitingStatList(){};

  ~WaitingStatList(){ elt.clear();}

  /**
   * add a new waiting data 
   * @param e the waiting data to be added
   * @return  -1 if the added element cannot be used yet 
   * (in which case it is added to the waiting list), and its index
   * in the list if it can be used.
   */
  int add(WaitingStat e);

  /**
   * Access the ith element in the waiting list.
   * @param i the index of the element in the waiting list. i must be a valid index.
   * @return le ith element of the waiting list.
   */
  WaitingStat operator[](int i){
    return elt[i];
  }

};

/**
 * This class is used to manage and to compute the statistics of the experiment.
 */

class Statistics {
public :
  int nbInitCell; // number of initial cells at D1
  int nbFinalCell; // number of final cells alive at the end of the experiment
  int nbDeath; // total number of deaths
  int nbOut; // total number of cell going out the image
  int nbDeathWithoutDiv; // number of cell death without any previous division
  int nbDeathWithDiv; // number of cell death after a previous division
  int nbTotalMitosis; // total number of divisions
  int nbNormalDiv;// number of normal cell division  (div in 2 cells without fusion after)
  int nbNonDichotimicMitosis; // number of cells dividing in more than 2 other cells
  int nbAbnormalDiv; // div > 2 OU div avec fusion après (div suivie d'une fusion)
  int nbFusions; // number of cell fusions
  int nbFirstDiv; // number of initial divisions
  int sumTimeFirstDiv; // summation of time stamps for each initial division
  int sumTimeBetweenDiv;// summation of time stamps between succesive divisions 
  int nbIntervalBetweenDiv;
  vector <int> divpercell;
  vector <int> idcellname;
  // v1.5.1
  int maxTime; // the higher time stamp in the event file
  int timeInterval; // the time interval between to shots 
  int nbDays; // number of days of the experiment
  
  // v1.1.3
  vector <int> nbLivingCell; // number of living cells to the end (END encountered) per initial cell
  int *nbMitosisPerDay;// number of mitosis for each day of the experiment
  int *nbDeathPerDay;// number of death for each day of the experiment
  int *nbAbnormalDivPerDay;
  int *nbNonDichotimicMitosisPerDay; 
  int *nbFusionPerDay;
  // v1.5.1
  int *nbFinalCellPerDay;
  
private:
  float nbMitoticCycle; // average number of division (number of division / number of initial cells )
  int averageCycleLength; // average time between two divisions
  int averageFirstCycleLength; // avergae time before the first division
  
public:
  Statistics() = delete; // disable the default constructor
  /**
   * Initialize the different statistics
   * @param maxTime the higher time stamp in the event file
   * @param timeInterval the time interval between to shots
   */
  Statistics(int maxTime, int timeInterval);

  ~Statistics();

  /**
   * Compute some average statistics. This function must be called before exporting
   ** statistics to CSV.
   */
  void computeAverages();

  /**
   * Export the statistics to a csv file
   * @param filename the name of the csv file
   * @return true if the export has been correctly done, false otherwise
   */
  bool toCSV(const std::string &filename, const vector <Name> &names);
  
  
};
#endif
