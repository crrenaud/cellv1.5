/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include "SvgExporter.hpp"
#include <sstream>
#include <ctime>
#include <cmath>

using namespace std;

// definition of some PI related constants
const float PI = 3.14159265359;
const float PIdiv6 = 0.523598775598;
const float PIdiv20 = 0.157079632679;

SvgExporter::SvgExporter(){
}


SvgExporter::~SvgExporter(){
  if(out.is_open()) close();
}


bool SvgExporter::open(const string &filename, const string &generator,
		       int w, int im, int days,
		       int mu, int mb, int ml, int mr){
  out.open(filename);
  if(!out.is_open()){
    cerr << "Impossible to create SVG output file "<< filename << endl;
    return false;
  }

  // initialization of page attributes
  pageWidth = w+ml+mr;
  pageHeight = im+mu+mb;

  mup = mu; mbottom = mb; mleft = ml; mright = mr;
  nbDays = days; nbImages = im;
  
  // header output
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
  out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" ";
  out <<"width=\"" << pageWidth << "\" height=\"" << pageHeight << "\">" << endl;

  // sortie de la description
  std::time_t result = std::time(nullptr);
  out << "<desc> Generated with "<< generator << " on " << std::asctime(std::localtime(&result)) << "</desc>" << endl;
  
  return true;
}

void SvgExporter::close(){
  if(out.is_open()){
    out << "</svg>" << endl;
    out.close();
  }
}


int SvgExporter::margin(Margin m){
  switch(m){
  case UP: return mup; break;
  case BOTTOM: return mbottom;
  case LEFT: return mleft; break;
  case RIGHT: return mright; break;
  }
  return 0;
}
      


void SvgExporter::background(const string &color){
  // margins drawing
  rect(pageWidth,mup,0, 0, "white");
  rect(pageWidth,mbottom,0,pageHeight-mbottom, "white");
  rect(mleft, pageHeight, 0, 0, "white");
  rect(mright,pageHeight, pageWidth-mright,0, "white");

  string colorlight, colordark;
  if(color=="grey"){
    colorlight = "rgb(230,230,230)";
    colordark = "rgb(192,192,192)";
  } else if(color=="red"){
    colorlight =  "mistyrose";
    colordark = "lightpink";
  } else if(color=="green"){
    colorlight = "palegreen";
    colordark = "mediumseagreen";
  } else if(color=="orange"){    
    colorlight = "peachpuff";
    colordark = "darksalmon";
  } else if(color=="yellow"){
    colorlight = "lemonchiffon";
    colordark = "wheat";
  } else if(color=="blue"){
    colorlight = "lightcyan";
    colordark = "skyblue";
  } else if(color=="violet"){   
     colorlight = "lavenderblush";
    colordark = "lavender";
  } else if(color=="white"){   
    colorlight = "snow" ;
    colordark = "ghostwhite";
  }else{
    cerr << "color " << color << " unknown - grey assumed ..." << endl;
    colorlight = "rgb(230,230,230)";
    colordark = "rgb(192,192,192)";
  }
  // division of the cells area into "days" vertical zones
  for(int i=0; i<nbDays; i++){
    string color=(i%2) ?   colorlight  : colordark ;
    rect(pageWidth-mleft-mright, (pageHeight-mup-mbottom)/nbDays,
	 mleft, mup+ i*(pageHeight-mup-mbottom)/nbDays, color);
  }

  int txtSize=10;

  // drawing days
  for(int i=0; i<nbDays; i++){
    stringstream str;
    str << "D" << i+1;
    text(mleft/2, mup+ (i+0.5)*(pageHeight-mup-mbottom)/nbDays, 10, str.str(), "red");
  }
    
  // drawing of the scale
  for(int i=0; i<=nbDays; i++){
    stringstream str;
    str <<  i*NB_IMAGES/NB_DAYS;
    text(mleft/2, mup+ i*(pageHeight-mup-mbottom)/nbDays+txtSize/2, txtSize, str.str(), "black");
  }
 
}


void SvgExporter::text(int x, int y, int size, const string &txt, const string &color){

  out << "<text x=\"" << x << "\" y=\"" << y << "\"" ;
  out << " font-family=\"Verdana\" font-size=\"" << size << "\"";
  out << " text-anchor=\"middle\" " ;
  out << " dominant-baseline=\"middle\" ";
  out << "fill=\"" << color << "\" > ";
  out << txt << "</text>" << endl;
}


void SvgExporter::line(int x1, int y1, int x2, int y2, const string &color){
  out << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\"";
  out << " x2=\"" << x2 << "\" y2=\"" << y2 << "\" stroke=\"" << color << "\"";
  out << " />" << endl;
}


void SvgExporter::rect(int width, int height, int x, int y, const string &color){
  out << "<rect width=\"" << width << "\" height=\"" << height << "\"";
  out << " x=\"" << x << "\" y=\"" << y << "\" fill=\"" << color << "\"";
  out << " />" << endl;
}

void SvgExporter::circle(int xc, int yc, int r, const string &color){
  out << "<circle cx=\"" << xc << "\" cy=\"" << yc << "\"";
  out << " r=\"" << r << "\" fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;
}

void SvgExporter::cross(int xc, int yc, int size, const string &color){
  line(xc-size/2, yc-size/2, xc+size/2, yc+size/2, color);
  line(xc-size/2, yc+size/2, xc+size/2, yc-size/2, color);
}

void SvgExporter::parallel(int xc, int yc, int size, const string &color){
  line(xc-size/2, yc+size/2, xc, yc-size/2, color);
  line(xc, yc+size/2, xc+size/2, yc-size/2, color);
}

void SvgExporter::diamond(int xc, int yc, int hsize, int vsize, const string &color){
  out << "<polygon points=\"" << xc+hsize << "," << yc << " ";
  out << xc << "," << yc+vsize << " ";
  out << xc-hsize << "," << yc << " ";
  out << xc << "," << yc-vsize << "\"";
  out << " fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;
}

void SvgExporter::ring(int xc, int yc, int rext, int rint, const string &color){
  out << "<circle cx=\"" << xc << "\" cy=\"" << yc << "\"";
  out << " r=\"" << rext << "\" fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;

  out << "<circle cx=\"" << xc << "\" cy=\"" << yc << "\"";
  out << " r=\"" << rint << "\" fill=\"" << "white" << "\" stroke=\"black\"";
  out << " />" << endl;
}

void SvgExporter::star(int xc, int yc, int radius, const string &color){
  float rdemi = radius/2.0;
  float angle = 0.0;

  out <<  "<polygon points=\"";

  for(int i=0; i<12; i++){// compuitng the 12 coordinates
    float r = (i%2==0) ? rdemi : radius;
    int x = xc+r*cos(angle);
    int y = yc+r*sin(angle);
    out << x << "," << y << " ";
    angle += PIdiv6;
  }

  out << "\" fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;
}

void SvgExporter::sun(int xc, int yc, int iradius, int eradius, const string &color){
  // generating sun's disk
  out << "<circle cx=\"" << xc << "\" cy=\"" << yc << "\"";
  out << " r=\"" << iradius << "\" fill=\"" << color << "\" stroke=\"black\"";
  out << " />" << endl;

  // generating sun's rays, spaced at an angle of pI/6
  float ircos = iradius*cos(PIdiv20);
  float irsin =  iradius*sin(PIdiv20);
  float xbase = (eradius-iradius)/5+iradius;
  float ybase = xbase*irsin/ircos;
  float angle=0.0;

  for(int i=0; i<12; i++){// generating 12 triangles, one per ray
    // writing the trinagle frome the image origin
    out << "<path d=\"M " << eradius << "," << "0 ";
    out << "L " << (int)xbase << "," << (int)ybase << " ";
    out << "L " << (int)xbase << "," << -(int)ybase << " ";
    out << "Z\" ";
    // moving the origin to the center of the sun's disk
    out << "transform =\"translate(" << xc << "," << yc << ") ";
    // rotate the triangle around the new origin 
    out << "rotate(" << angle*180/PI <<  ") \"";
    out << " fill=\"" << color << "\" stroke=\"black\"";
    out << " />" << endl; 

    angle += PIdiv6;
  }

}
