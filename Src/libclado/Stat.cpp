/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include "Stat.hpp"
#include <iostream>
#include <fstream>
using namespace std;

//extern vector <Name> names;

WaitingStat:: WaitingStat(){
  pcell = nullptr;
  nbParentsEncountered=0;
}

WaitingStat:: WaitingStat(Node *pcell){
  this->pcell = pcell;
  nbParentsEncountered=0;
}

bool WaitingStat::isready(){ return nbParentsEncountered==pcell->getNbParents();}

int WaitingStatList::add(WaitingStat e){
  int id=-1;
  // we check if the element already exists
  for(int i=0; i<elt.size(); i++)
    if(e.pcell == elt[i].pcell){// the element exists
      elt[i].nbParentsEncountered++;
      id = i;
      break;
    }

  if(id==-1){// The element does not exist or the list is empty 
      elt.push_back(e);
      elt[elt.size()-1].nbParentsEncountered++;
      return -1;// the item cannot be ready because only one parent 
  }
   // the element exists; we verify it can be used
  if(elt[id].isready()) return id;
  return -1;
}

Statistics::Statistics(int mt, int ti){

  maxTime = mt;
  timeInterval = ti;
  // compute the number of days for the experiment
  int nbImagesPerDay = 1440 / timeInterval;
  nbDays = maxTime / nbImagesPerDay;
  if(maxTime % nbImagesPerDay){
    nbDays++;
    maxTime = nbDays*nbImagesPerDay;
  }

  // init local stats
  nbInitCell=0;
  nbFinalCell=0;
  nbDeath=0;
  nbOut=0;
  nbDeathWithoutDiv=0;
  nbDeathWithDiv=0;
  nbTotalMitosis=0;
  nbMitoticCycle=0;
  nbNormalDiv=0;
  nbAbnormalDiv=0;
  nbNonDichotimicMitosis=0;
  nbFusions=0;
  averageCycleLength=0;
  averageFirstCycleLength=0;
  nbFirstDiv=0;
  sumTimeFirstDiv=0;
  nbIntervalBetweenDiv=0;
  sumTimeBetweenDiv=0;

  // V1.5.1
  // allocate days stats tables
  nbMitosisPerDay = new int[nbDays];
  nbDeathPerDay = new int[nbDays];
  nbAbnormalDivPerDay = new int[nbDays];
  nbNonDichotimicMitosisPerDay = new int[nbDays];
  nbFusionPerDay = new int[nbDays];
  nbFinalCellPerDay = new int[nbDays];
  
  for(int i=0; i<nbDays; i++){
    nbMitosisPerDay[i] = 0;
    nbDeathPerDay[i] = 0;
    nbAbnormalDivPerDay[i] = 0;
    nbNonDichotimicMitosisPerDay[i] = 0;
    nbFusionPerDay[i] = 0;
    nbFinalCellPerDay[i] = 0;
  }
}

Statistics::~Statistics(){
  delete nbMitosisPerDay;
  delete nbDeathPerDay;
  delete nbAbnormalDivPerDay;
  delete nbNonDichotimicMitosisPerDay;
  delete nbFusionPerDay;
}

  
bool Statistics::toCSV(const string &filename, const vector <Name> &names){
  ofstream out(filename);

  if(!out.is_open()){
    cerr << "unable to save statistics in " << filename << endl;
    return false;
  }

#ifdef FRENCH
  out << "Nombre de cellules initiales," << nbInitCell << endl;
  out << "Nombre de cellules finales," << nbFinalCell << endl;

  out << "Nombre de mitoses totale," << nbTotalMitosis << endl;
  out << "Nombre de cycles mitotiques," << nbMitoticCycle << endl;
  out << "Nombre de mitoses normales," << nbNormalDiv << endl;
  out << "Nombre de mitoses anormales," << nbAbnormalDiv << endl;
  out << "Nombre de mitoses non dichotomiques," << nbNonDichotimicMitosis  << endl;
  out << "Nombre de refusions après mitose," << nbFusions <<  endl;

  out << "Nombre total de morts," << nbDeath << endl;
  out << "Nombre de mort sans mitose," << nbDeathWithoutDiv << endl;
  out << "Nombre de mort après mitose," <<  nbDeathWithDiv << endl;
  out << "Nombre de sorties," << nbOut << endl;

  out << "Longueur moyenne entre deux cycles," << averageCycleLength << endl;
  out << "Longueur moyenne entre T0 et première division," << averageFirstCycleLength << endl;
#else
  out << "Initial number of cells," << nbInitCell << endl;
  out << "Final number of cells," << nbFinalCell << endl;
  

  out << "Total number of mitoses," << nbTotalMitosis << endl;
  out << "Number of mitotic cycles," << nbMitoticCycle << endl;
  out << "Number of normal mitoses," << nbNormalDiv << endl;
  out << "Number of abnormal mitoses," << nbAbnormalDiv << endl;
  out << "Number of non-dichotomous mitoses," << nbNonDichotimicMitosis  << endl;
  out << "Number of re-fusions after mitosis," << nbFusions <<  endl;

  out << "Total number of deaths," << nbDeath << endl;
  out << "Number of deaths without mitosis," << nbDeathWithoutDiv << endl;
  out << "Number of deaths after mitosis," <<  nbDeathWithDiv << endl;
  out << "Number of cell exits," << nbOut << endl;

  out << "Average length between two cycles," << averageCycleLength << endl;
  out << "Average length between T0 and first division," << averageFirstCycleLength << endl;
#endif

  // Sortie des statistiques par cellule initiale

  out << "," << endl;
#ifdef FRENCH
  out << "Nom des cellules,";
#else
  out << "Name of cells,";
#endif
  for(int i=0; i<divpercell.size(); i++){
    out << names[idcellname[i]].getName();
    if(i<divpercell.size()-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de divisions,";
#else
  out << "Number of divisions,";
#endif
  for(int i=0; i<divpercell.size(); i++){
    out << divpercell[i];
    if(i<divpercell.size()-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de cellules finales,";
#else
  out << "Number of final cells,";
#endif
for(int i=0; i<nbLivingCell.size(); i++){
    out << nbLivingCell[i];
    if(i<nbLivingCell.size()-1) out << ",";
  }
  out << endl;

  // output of distributions for statistics by initial cell
  int *cpt;
  int max;
  
  out << "," << endl;
#ifdef FRENCH
  out << "Nombre de divisions constaté par cellule intiale,";
#else
  out << "Number of divisions found,";
#endif
  // search for the maximum number of subdivisions
  max=0;
  for(int i=0; i<divpercell.size(); i++)
    if(divpercell[i]>max) max = divpercell[i];
  for(int i=0; i<=max; i++){
    out << i;
    if(i<max) out << ",";
  }
  
  out << endl;

#ifdef FRENCH
  out << "Nombre de cellules concernées,";
#else
  out << "Number of cells involved,";  
#endif
  cpt = new int[max+1];
  for(int i=0; i<=max; i++) cpt[i]=0;
  for(int i=0; i<divpercell.size(); i++)
    cpt[divpercell[i]]++;
  for(int i=0; i<=max; i++){
    out << cpt[i];
    if(i<max) out << ",";
  }
  out << endl;
  delete cpt;
  

  out << "," << endl;
#ifdef FRENCH
  out << "Nombre de cellules finales par cellule initiale,";
#else
  out << "Number of final cells found,";
#endif

  // search for the maximum number of final cells
  max=0;
  for(int i=0; i<nbLivingCell.size(); i++)
    if(nbLivingCell[i]>max) max = nbLivingCell[i];
  for(int i=0; i<=max; i++){
    out << i;
    if(i<max) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de cellules concernées,";
#else
  out << "Number of cells involved,";
#endif
  cpt = new int[max+1];
  for(int i=0; i<=max; i++) cpt[i]=0;
  for(int i=0; i<nbLivingCell.size(); i++)
    cpt[nbLivingCell[i]]++;
   for(int i=0; i<=max; i++){
    out << cpt[i];
    if(i<max) out << ",";
   }
   out << endl;
  delete cpt;

  // output of statistics per day
  
  out << "," << endl;
#ifdef FRENCH
out << "Jours, ";;
  for(int i=1; i<nbDays; i++) out << "D" << i << " ,";
  out << "D" << nbDays << endl;
  out << "Nombre de morts par jour,";
#else
out << "Days, ";
  for(int i=1; i<nbDays; i++) out << "D" << i << " ,";
  out << "D" << nbDays << endl;
  out << "Number of deaths per day,";
#endif

  for(int i=0; i<nbDays; i++){
    out << nbDeathPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de mitoses par jour,";
#else
  out << "Number of mitoses per day,";
#endif
  for(int i=0; i<nbDays; i++){
    out << nbMitosisPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;  
#ifdef FRENCH
  out << "Nombre de mitoses anormale par jour ,";
#else
  out << "Number of abnormal mitoses per day ,";
#endif
  for(int i=0; i<nbDays; i++){
    out << nbAbnormalDivPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de mitoses non dichotomiques par jour ,";
#else
  out << "Number of non-dichotomous mitoses per day ,";
#endif
  for(int i=0; i<nbDays; i++){
    out << nbNonDichotimicMitosisPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de fusions par jour ,";
#else
  out << "Number of fusions per day,";
#endif
  for(int i=0; i<nbDays; i++){
    out << nbFusionPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;
#ifdef FRENCH
  out << "Nombre de cellules à la fin du jour ,";
#else
  out << "Number of cells at the end of day,";
#endif
  for(int i=0; i<nbDays; i++){
    out << nbFinalCellPerDay[i];
    if(i<nbDays-1) out << ",";
  }
  out << endl;
  
  out.close();

  return true;
}

void Statistics::computeAverages(){
  if(nbInitCell)
    nbMitoticCycle = (nbTotalMitosis)/(float)nbInitCell;
  if(nbFirstDiv)
    averageFirstCycleLength = sumTimeFirstDiv /  nbFirstDiv;
  if(nbIntervalBetweenDiv)
    averageCycleLength = sumTimeBetweenDiv / nbIntervalBetweenDiv;

}
