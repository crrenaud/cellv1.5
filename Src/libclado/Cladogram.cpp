/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "Cladogram.hpp"
#include "Node.hpp"
#include "SvgExporter.hpp"

#define WIDTH_CELL  50 /**< width in pixels for the display of each terminal cell */
#define PAGE_HEIGHT 500 /**< height of the image in number of pixels */

// classes and structures usefull for managing some cladogram's informations

// class for handling and displaying exceptions
class readException : public exception {
private:
  string mess;
public:
  readException(const string &s){ mess = s; }
  string what()  { return mess;}
};

// association between à Mark and its string representation in
// the text file
struct MarkPair {
  string name;
  Mark value;
};
// initialization of the associations
static const vector <struct MarkPair> marks={{"diamond", DIAMOND}, {"ring", RING}, {"star",STAR}, {"sun", SUN}};
// autorized colors
static const vector<string> colors={"grey", "red", "green", "orange", "yellow", "blue", "violet", "white"};


Cladogram::Cladogram(){
  // initialize default color
  color = "grey";
  // initialize higher event date (unknown)
  maxTime = -1;
  // initialize time interval between frames (unknown)
  timeInterval = -1;
}

Cladogram::~Cladogram(){
  // memory cleaning
  names.clear();
  cells.clear();
}

bool Cladogram::load(const string &filename){
  cout << "filename = " << filename << endl;

  // Opening data file
  ifstream in(filename);
  if(!in.is_open()){
    cerr << "Error opening " << filename << endl;
    return false;
  }

  // opening log file
  bool log=true;
  ofstream output(getLogFilename(filename));
  if(!output.is_open()){
    cerr << "Error opening log file " << filename << endl;
    log=false;
  }

   // reading the entire file of events
  
  string stime; // event's time
  in >> stime;
  
  while(!in.eof()){// reading an event
    
    // detection of a comment line
    if(stime[0]=='#'){
      getline(in, stime);
      in >> stime;
      continue;
    }

    if(stime=="color"){
      in >> color;
      in >> stime;
      if(log) output << "color = " << color << endl;
      continue;
    }

    try {
      
      if(stime=="interval"){
	int time;
	in >> time;
	if(timeInterval!=-1){ // already found in the file
	  stringstream err;
	  err << "time interval defined more than once : interval " << time ;
	  throw readException(err.str());
	}
	if(time<=0){// bad time interval value
	  stringstream err;
	  err << "bad time interval : interval " << time ;
	  throw readException(err.str());
	}
	timeInterval = time;
	in >> stime;
	if(log) output << "timeInterval = " << timeInterval << endl;
	continue;
      }
    
      // decoding the time stamp in stime
      int time = atoi(stime.c_str());

      // retrieve the event
      string event; // type of event
      in>> event;

      // event type analysis

      if(event=="begin"){// new cell
	begin(in, time, log, output);
       }else if(event=="end"){// end of image sequence for the cell
	end(in, time, log, output);
      }else if(event=="death"){// death of cell
	death(in, time, log, output);
      }else if(event=="out"){// output of cell
	out(in, time, log, output);
      }else if(event=="div"){// division of cell
	div(in, time, log, output);
      }else  if(event=="fusion"){// fusion of cells
	fusion(in, time, log, output);
      } else {// unknown event
	stringstream err;
	err << "unknown event : " << event;
	throw readException(err.str());
      }
    } catch (readException &e){
      cout << "Error - " << e.what() << endl;
      names.clear();
      cells.clear();
      in.close();
      if(log){
	output << "Error - " << e.what() << endl;
	output.close();
      }
      return false;
    }
    
    // reading next event
    in >> stime;
  }

  if(timeInterval==-1) // unknown
    timeInterval = 10; // default value 10 minutes
  
  
  // reading was ok
  in.close();
  if(log) output.close();
  return true;

}

void Cladogram::print(const string &filename, const string &generator){
  // recursive calculation of the number of final cells
  int nbcol=0;// nb of final cells
  for(int i=0; i<cells.size(); i++){
    nbcol += cells[i]->width(1);
  }

  // calculation of the width of the image
  int width = nbcol*WIDTH_CELL;

  // compute the number of days for the experiment
  int nbImagesPerDay = 1440 / timeInterval;
  int nbDays = maxTime / nbImagesPerDay;
  if(maxTime % nbImagesPerDay){
    nbDays++;
    maxTime = nbDays*nbImagesPerDay;
  }
  
  cout << "maxtime = " << maxTime << " days = " << nbDays << endl;
  
  // --- export to svg format ---
  SvgExporter exporter;

  //  if(!exporter.open(filename, generator, width, 721)){
  if(!exporter.open(filename, generator, width, maxTime+1, nbDays)){
    cerr << "error when exporting to svg" << endl;;
    return;
  }
  // setting the background color
  exporter.background(color);


  // printing each cell tree to SVG
  nbcol = 0;
  for(int i=0; i<cells.size(); i++){
    int cwidth = cells[i]->width(1);
    cells[i]->tosvg(exporter, nbcol*WIDTH_CELL+exporter.margin(LEFT), cwidth*WIDTH_CELL, names );
    nbcol+=cwidth;
  }

  // closing the SVG file
  exporter.close();
}

void Cladogram::printStats(const string &csvfilename){
  Statistics stat(maxTime, timeInterval);
  for(int i=0; i<cells.size(); i++){
    bool divActive=false; // <- à revoir !!!
    cells[i]->collectStatistics(stat, false, -1);
  }
  stat.computeAverages();    
  stat.toCSV(csvfilename, names);
}

bool Cladogram::compare(const Cladogram &c){
  // comparison of cladogram's size
  // a different size means that the two cladograms
  // are necessarily different
  if(cells.size() != c.cells.size()) return false;

  // compararison of each cell tree
  for(int i=0; i<cells.size(); i++)
    if(!cells[i]->compare(c.cells[i])) return false;
  
  return true;
}

string Cladogram::getLogFilename(const string &evtfilename){
  return string("out.log");
}

string Cladogram::getSvgFilename(const string &evtfilename){
  string svgfilename;
  int pos = evtfilename.find_last_of('.');
  if(pos== string::npos){// no extension
    svgfilename = evtfilename;
  }else{
    svgfilename = evtfilename.substr(0,pos);
  }
  svgfilename.append(".svg");
  return svgfilename;

}

string Cladogram::getCsvFilename(const string &evtfilename){
  string csvfilename;
  int pos = evtfilename.find_last_of('.');
  if(pos== string::npos){// no extension
    csvfilename = evtfilename;
  }else{
    csvfilename = evtfilename.substr(0,pos);
  }
  csvfilename.append(".csv");
  return csvfilename;
}

void Cladogram::begin(ifstream &in, int time, bool log, ofstream &out){
 string name;
  in >> name;
  if(exist(name)>=0){
    stringstream err;
    err << "cell " << name << " still defined ... cannot use begin" << endl;
    throw readException(err.str());
  } else {
    if(log) out << "cell " << name << " created" << endl;
    Node *ncell = new Node(names.size(), time);
    Name nname(name, ncell);
    names.push_back(nname);
    cells.push_back(ncell);
  }
}

void Cladogram::end(ifstream &in, int time, bool log, ofstream &out){
  // update maxTime
  if(time > maxTime) maxTime = time;

  // reading parameters
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){// cell name is valid

    // verifying whether on optional mark is following
    string mark;
    in >> mark;
    if(in.eof()){// special case of the last end without mark
      names[loc].getNode()->setEnd(END, time);// CIRCLE by default
      if(log) out << "cell " <<name << " end of sequence" << endl;
      return;
    }
    // searching for a valid mark
    for(int i=0; i<marks.size(); i++){
      if(marks[i].name==mark){
	// reading and analysing the color of the mark
	string color;
	in >> color;
	int icol=-1;
	// verifying the validity of the color
	for(int i=0; i<colors.size(); i++){
	  if(colors[i]==color){
	    icol=i; break;
	  }
	}// for
	if(icol>=0){
	  // store the end and the mark
	  if(log) out << "cell " << name << " ended - marked with " << mark << " " << color << endl;
	  names[loc].getNode()->setEnd(END, time, marks[i].value, color);
	  return;
	}else{ // color name is not valid
	  stringstream err;
	  err << "end : cell " << name << " ended with a bad mark's color (" << color << ")";
	  throw readException(err.str());
	}
	return;
      }
    }
    // Th mark is not valid. We must verify whether the content of "mark" is a date event
    // or a comment. In these cases the content is "put back" to the stream for the following
    // read. Otherwise an error occurs and we stop
    if(!isTime(mark) && mark[0]!='#'){ // syntax error
      stringstream err;
      err << "end : cell " << name << " mark (" << mark << ") undefined... ";
      throw readException(err.str());
    } else { // no mark - CIRCLE by default
      names[loc].getNode()->setEnd(END, time);
      if(log) out << "cell " <<name << " end of sequence" << endl;
      // put back the time string in the stream for future analysis
      int pos = in.tellg();
      in.seekg(pos-mark.length(), in.beg);   
    }
  } else {
    stringstream err;
    err << "end : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

void Cladogram::out(ifstream &in, int time, bool log, ofstream &output){
  // update maxTime
  if(time > maxTime) maxTime = time;

  // reading parameters
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){
    names[loc].getNode()->setEnd(OUT, time);
    if(log) output << "cell " << name << " out of image at  " << time << endl;
  } else {
    stringstream err;
    err << "out : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

void Cladogram::death(ifstream &in, int time, bool log, ofstream &out){
  // update maxTime
  if(time > maxTime) maxTime = time;

  // reading parameters
  string name;
  in >> name;
  int loc = exist(name);
  if(loc>=0){
    names[loc].getNode()->setEnd(DEAD, time);
    if(log) out << "cell " << name << " is dead at time " << time << endl;
  } else {
    stringstream err;
    err << "death : cell " << name << " undefined... ";
    throw readException(err.str());
  }
}

void Cladogram::div(ifstream &in, int time, bool log, ofstream &out){
  string name;
  in >> name;
  int icell = exist(name);
  if(icell>=0){
    int nbdiv;
    in >> nbdiv;
    if(log) out << "cell " << name << " divides in " << nbdiv <<  " cells at time " << time << endl;
    // recovery of the cell
    Node *dcell = names[icell].getNode();
    dcell->setEnd(DIV, time);
    dcell->createSons(nbdiv);
    for(int i=0; i<nbdiv; i++){
      in >> name;
      if(exist(name)>=0){
	stringstream err;
	err << "division : name " << name << " still exists...";
	throw readException(err.str());
	return;
      }
      Node *ncell = new Node(names.size(), time);
      Name nname(name, ncell);
      names.push_back(nname);
      if(dcell->addSon(ncell)){
	if(log) out << "div : cell named " << name << " created" << endl;
      } else {
	stringstream err;
	err << "div : error when adding son " << name;
	throw readException(err.str());
	return;
      }
    }
  } else {
    stringstream err;
    err << "div : cell name " << name << " undefined";
    throw readException(err.str());
  }
}

void Cladogram::fusion(ifstream &in, int time, bool log, ofstream &out){
  vector <int> id;
  // recovery of the cells to be merged
  string name;
  in >> name;
  while(name!="in"){
    int cellid=exist(name);
    if(cellid<0){
      stringstream err;
      err << "error : cell " << name << " undefined !!!";
      throw readException(err.str());
      return;
    }
    // the cell exists
    if(log) out << "cell " << name << " found" << endl;
    id.push_back(cellid);
    // recovery of the next cell
    in >> name;
  }// while

  // retrieve the name of the merged cell
  in >> name;
  if(exist(name)>=0){
    stringstream err;
    err << "error : cell " << name << " still exists...";
    throw readException(err.str());
    return;
  }

  // apply the merge 
  //input: we have the id of the cells to merge
  //create the merged cell
  Node *ncell = new Node(names.size(), time);
  Name nname(name, ncell);
  names.push_back(nname);

  // update the merged cells
  for(int i=0; i<id.size(); i++){
    Node *pcell = names[id[i]].getNode();
    pcell->setEnd(FUSION, time);
    pcell->createSons(1);
    pcell->addSon(ncell);
  }
}

int Cladogram::exist(const string &name){
  for(int i=0; i< names.size(); i++)
    if(name == names[i].getName()) return i;
  return -1;
}

bool Cladogram::isTime(const string &s){
  for(int i=0; i<s.length(); i++)
    if(!isdigit(s[i])) return false;
  return true;
}
