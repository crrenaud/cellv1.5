/*
  This file is part of Toto-Cell, a software package that represents
  the evolution of a cell population after a video-microscopy experiment
  through a cladogram and provides statistical data on this evolution.

  Copyright (c) 2021-2024 by Christophe RENAUD.

  Cell is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

//#include "Node.hpp"
//#include "Input.hpp"
//#include "SvgExporter.hpp"
//#include "Stat.hpp"
#include "./libclado/Cladogram.hpp"

/**
 * toto-cell is a software that allows you to read back data
 * from a cell evolution sequence analysis and produce a graph in SVG format. 
 */


#define VERSION "1.5.1"
// Updated from version 1.4
// date : january 2024
// - creation of the cladogram class 
// - creation on "interval" command
// output of stats in english



// pre-defining functions
void copyright();

/**
 * main function. 
 * @param argc number of arguments of the command (must be 2)
 * @param argv list of arguments of the commands. 
 * The syntax of the command must be : <commandName>  <event file name>
 */
int main(int argc, char *argv[]){
  // printing the copyright
  copyright();
  
  //verifying parameters
  if(argc != 2){
    cerr << "Syntax = " << argv[0] << " <event file name>" << endl;;
    return 0;
  }


  
  // generate output file names from event file name
  // the two output files are :
  // - the SVG image
  // - the statistics file (scv format)
  string evtfilename(argv[1]);
  string svgfilename, csvfilename;
  
  int pos = evtfilename.find_last_of('.');
  if(pos== string::npos){// no extension
    svgfilename = evtfilename;
    csvfilename = evtfilename;
  }else{
    svgfilename = evtfilename.substr(0,pos);
    csvfilename = evtfilename.substr(0,pos);
  }

  svgfilename.append(".svg");
  csvfilename.append(".csv");

  // compute the string that represents the name and version of the software
  stringstream generator;
  string exe(argv[0]);
  size_t found = exe.find_last_of('/');
  if(found!=string::npos) generator << exe.substr(found+1);
  else generator << exe;
  generator << " v" << VERSION;

  // building the cladogam from the data
  Cladogram clado;
  if(clado.load(argv[1])==true){// loading has been correcly done

    // print the SVGfile
    clado.print(svgfilename, generator.str());
    // print the statistics
    clado.printStats(csvfilename);

    // end message
    cout << "Cell ended correctly" << endl;
    return 1;
  }else{
    return 0;
  }


  
}


/**
 * Function that prints the toto cell copyright
 */
void copyright(){
  cout << "Cell V" << VERSION;
  cout << " - Copyright C. RENAUD 2021-2024" << endl;
}
